/*
 * Idle.cpp
 *
 *  Created on: May 12, 2018
 *      Author: OS1
 */

#include "Idle.h"
#include "PCB.h"
#include "Kernel.h"

Idle::Idle():Thread(4096,1) {}

Idle::~Idle() {}

void Idle::run(){
	while(1) {}
}

void Idle::start(){
	Kernel::listOfPCB->getByID(myPCB)->state = PCB::READY;
	Kernel::listOfPCB->getByID(myPCB)->initStack();
	//ne stavlja se u Scheduler
}
