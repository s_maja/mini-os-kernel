/*
 * Queue.cpp
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */

#include "Queue.h"
#include "Kernel.h"


Queue::Queue() {
	first = NULL;   //lock
	last = NULL;
	len = 0;
}

Queue::~Queue() {
	while (first != NULL) {
		Elem* old = first;
		first = first->next;
		delete old;
	}
	first = NULL;
	last = NULL;
}

PCB* Queue::get() {
	PCB* pcb=NULL;
	if (first == NULL) {

		return NULL;
	} else {
		Elem* newElem = first;
		first = first->next;
		newElem->next = NULL;
		pcb=newElem->info;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
		delete newElem;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
		if (first == NULL)
			last = NULL;
		len--;
		return pcb;
	}
}

void Queue::put(PCB* pcb) {
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	Elem * newElem = new Elem(pcb);
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if (first == NULL)
		first = newElem;
	else
		last->next = newElem;
	last = newElem;
	len++;
}

int Queue::empty() {
	if (len == 0)
		return 1;  //ako vraca 1 red je prazan
	 else
		return 0;
}

PCB* Queue::getFirst() {
	return first->info;
}

int Queue::getLen() {
	return len;
}
