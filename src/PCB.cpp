/*
 * PCB.cpp
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */

#include "PCB.h"
#include "Kernel.h"
#include "SCHEDULE.H"

ID PCB::PID=0;

PCB::PCB(Thread *thread, StackSize size, Time time){
	id=++PID;
	myThread=thread;
	stackSize=size;
	timeSlice=time;
	timePassed=0;
	timeToSleep=0;
	state= NEW;
	waitingThread= new Queue();
	Kernel::listOfPCB->insertBack(this);
	sp=ss=0;stack=NULL;
}

PCB::~PCB(){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete waitingThread;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	if(stack) delete[] stack;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
}

void PCB::initStack(){
	unsigned int s= stackSize/2 + (stackSize%2);

	stack= new unsigned int[s];
	//pravimo vjestacki pocetni kontekst da bi se pri prvom dispatch-u imalo sta skinuti sa steka

#ifndef BCC_BLOCK_IGNORE
	stack[s-1]= FP_SEG(myThread);//cuvamo parametar za wrapper fju
	stack[s-2]= FP_OFF(myThread);
	stack[s-5] = 0x200;//setovan I fleg u pocetnom PSW-u za nit
	stack[s-6] = FP_SEG(&(Thread::wrapper));
	stack[s-7] = FP_OFF(&(Thread::wrapper));

	this->sp = FP_OFF(stack+s-16); //svi sacuvani registri pri ulasku u interrupt rutinu
	this->ss = FP_SEG(stack+s-16);
#endif

}

void PCB::start(){
	initStack();
	state=PCB::READY;
	if(myThread->myPCB!=2)
		Scheduler::put(this);
}

void PCB::sleep(unsigned int timeToSleep){
	if(timeToSleep <= 0) return;
	Kernel::running->state=PCB::BLOCKED;
	Kernel::running->timeToSleep=timeToSleep;
	Kernel::sleepingThreads->insertSort((PCB*)Kernel::running);
	Kernel::dispatched=1;
}

void PCB::waitToComplete(){
	if((this->myThread ==Kernel::idleThread)||   //nema wait na idle thread
	  ((PCB*)Kernel::running== this)||	//nema wait nad samim sobom
	  (this->myThread == Kernel::startingThread)|| 	// nema wait nad starting thread
	  (this->state== PCB::OVER)||(this->state== PCB::NEW))
			return;

		Kernel::running->state = PCB::BLOCKED;		//postavi da je nit bokirana
		this->waitingThread->put((PCB*)Kernel::running);	//stavi running u red blokiranih nad this->Kernel::listOfPCB->getByID(myPCB)
		Kernel::dispatched=1; //zamjeni kontekst jer je running sad block
}
