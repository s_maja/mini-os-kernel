/*
 * Main.cpp
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */
#include  "Kernel.h"
#include "stdio.h"

int userMain(int argc, char** argv);

int main(int argc, char* argv[]){
	Kernel::inic();
	int ret= userMain(argc,argv);
	Kernel::restore();
	return ret;
}



