/*
 * Kernel.cpp
 *
 *  Created on: May 14, 2018
 *      Author: OS1
 */

#include "Kernel.h"
#include "PCB.h"
#include "SCHEDULE.H"

void tick();

volatile PCB* Kernel::running = NULL;
Idle* Kernel::idleThread = NULL;
KerThr* Kernel::runningKernelThread=NULL;
Thread* Kernel::startingThread = NULL;
volatile int Kernel::dispatched = 0;
InterruptFunc Kernel::oldTimer = NULL;
List* Kernel::listOfPCB=NULL;
ListSem* Kernel::listOfKernelSem=NULL;
ListEve* Kernel::listOfKernelEve=NULL;
List* Kernel::sleepingThreads=NULL;
SystemData* Kernel::globalSysData=NULL;
volatile int Kernel::softLock=0;

Kernel::~Kernel() {
	if (idleThread) delete idleThread;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	if (runningKernelThread) delete runningKernelThread;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	if (startingThread) delete startingThread;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete listOfPCB;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete sleepingThreads;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete listOfKernelSem;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete listOfKernelEve;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
}

void interrupt Kernel::timerInterrupt(...) {
	static volatile PCB* thread;
	static volatile unsigned int timerSP, timerSS;

	tick();
	(*oldTimer)();

	Kernel::sleepingThreads->decAll();

	if (running->timeSlice == 0)
		return;


	running->timePassed++;
	if (running->timePassed < running->timeSlice)
		return;

	if(Kernel::softLock)
		Kernel::dispatched=1;

if(softLock==0){
	if ((running->state == PCB::READY) && (running != (volatile PCB*)Kernel::listOfPCB->getByID(idleThread->myPCB)))
		Scheduler::put((PCB*) running);

	while(1){
		thread = Scheduler::get();
		if((thread==NULL) ||(thread->state==PCB::READY))break;
	}

	if (thread == NULL)
		thread = (volatile PCB*)Kernel::listOfPCB->getByID(idleThread->myPCB);

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov timerSP, sp
		mov timerSS, ss
	}

	running->sp = timerSP;
	running->ss = timerSS;
	running = thread;
	timerSP = running->sp;
	timerSS = running->ss;

	asm {
		mov sp, timerSP
		mov ss, timerSS
	}
#endif

	running->timePassed=0;
}
}

void Kernel::inic() {
#ifndef BCC_BLOCK_IGNORE
	lock();
oldTimer= getvect(0x08); //dohvatam staru
setvect(0x60,oldTimer);//cuvam je na 0x60
setvect(0x08, timerInterrupt);//postavljam novu na 0x08

setvect(0x61,SysCallRout); // postavljamo prekidny za sistemske pozive u 61h
setvect(0x63,BackToUserRun);//postavljamo prekidnu pomocu koje cemo se vratiti iz kernel moda

	unlock();
#endif

#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	listOfPCB=new List();  //pravim listu PCB
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif

#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	listOfKernelSem=new ListSem();  //pravim listu Kernel semafora
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif

#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	listOfKernelEve=new ListEve();  //pravim listu Kernel eventa
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif

#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	sleepingThreads=new List();  //pravim listu uspavanih niti
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif

#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	startingThread = new Thread(-1); //pravim pocetnu nit id=1
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	Kernel::listOfPCB->getByID(1)->state = PCB::READY; // startingThread->myPCB->state = PCB::READY;
	Kernel::running = (volatile PCB*)Kernel::listOfPCB->getByID(1); // startingThread->myPCB; sa konverzijom
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	runningKernelThread = new KerThr(); //pravio kernel nit  id=2
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	Kernel::listOfPCB->getByID(2)->start();

idleThread = new Idle();
idleThread->start();
}

void Kernel::restore() {
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	if (running != (volatile PCB*)Kernel::listOfPCB->getByID(1))
		{
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
		return;
		}
#ifndef BCC_BLOCK_IGNORE
oldTimer=getvect(0x60);
setvect(0x08,oldTimer);
#endif
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
}

void interrupt Kernel::SysCallRout(...){
	static volatile unsigned int tcx, tdx;
	static volatile unsigned int timerSP, timerSS;

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov timerSP, sp
		mov timerSS, ss
	}

	Kernel::running->sp = timerSP;
	Kernel::running->ss = timerSS;

	asm{
		mov tcx,cx;
		mov tdx,dx;
	}
	globalSysData = (SystemData*)MK_FP(tcx, tdx);

	timerSP=Kernel::listOfPCB->getByID(Kernel::runningKernelThread->myPCB)->sp;
	timerSS=Kernel::listOfPCB->getByID(Kernel::runningKernelThread->myPCB)->ss;

	asm {
		mov sp, timerSP
		mov ss, timerSS
	}
#endif
}

void interrupt Kernel::BackToUserRun(...){
	static volatile unsigned int timerSP, timerSS;
	static volatile PCB* thread;

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov timerSP, sp
		mov timerSS, ss
	}

	Kernel::listOfPCB->getByID(Kernel::runningKernelThread->myPCB)->sp=timerSP;
	Kernel::listOfPCB->getByID(Kernel::runningKernelThread->myPCB)->ss=timerSS;

	timerSP = Kernel::running->sp;
	timerSS = Kernel::running->ss;

		asm {
			mov sp, timerSP
			mov ss, timerSS
		}
#endif
		Kernel::softLock=0;

if(dispatched){  //ako je zahtjevana promjena konteksta promjeni kontekst
	dispatched=0;
	if ((running->state == PCB::READY) && (running != (volatile PCB*)Kernel::listOfPCB->getByID(idleThread->myPCB)))
			Scheduler::put((PCB*) running);

	while(1){
		thread = Scheduler::get();
		if((thread==NULL) ||(thread->state==PCB::READY))break;
	}

	if (thread == NULL){
		thread = (volatile PCB*)Kernel::listOfPCB->getByID(idleThread->myPCB);
	}
	#ifndef BCC_BLOCK_IGNORE
		asm {
			mov timerSP, sp
			mov timerSS, ss
		}

		running->sp = timerSP;
		running->ss = timerSS;
		running = thread;
		timerSP = running->sp;
		timerSS = running->ss;

		asm {
			mov sp, timerSP
			mov ss, timerSS
		}
	#endif

	running->timePassed=0;
 }
}
