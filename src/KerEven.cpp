/*
 * KerEven.cpp
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#include "KerEven.h"
#include "Kernel.h"
#include "SCHEDULE.H"
#include "IVTEn.h"

class IVTEntry;

int KernelEv::ID=0;

KernelEv::KernelEv(IVTNo ivtNo){
	if(IVTEntry::IVTable[ivtNo]!=NULL) {
	id=++ID;
	ivtNum=ivtNo;
	IVTEntry::setEvent(this,ivtNo);
	owner=(PCB*)Kernel::running;
	val=0;
	Kernel::listOfKernelEve->insertBack(this);
	}
}

KernelEv::~KernelEv(){
	signal();
	owner=NULL;
}

void KernelEv::wait(){
	if(Kernel::running==(volatile PCB*)owner) {
		if(val==0){
			owner->setState(PCB::BLOCKED);
			Kernel::dispatched=1;
		}
		else
			val=0;
	}
}

void KernelEv::signal(){
	if(owner->state==PCB::READY)
		val=1;
	else{
		owner->setState(PCB::READY);
		Scheduler::put(owner);
	}
	if (Kernel::softLock)
		Kernel::dispatched = 1;
	else
		dispatch();
}
