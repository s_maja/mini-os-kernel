/*
 * Event.cpp
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#include "KerEven.h"
#include "Kernel.h"

Event::Event(IVTNo ivtNo){
	SystemData data;
	data.name=data.EVE_CONS;
	data.ivtNo=ivtNo;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
			push cx;
			push dx;
			mov cx, tcx;
			mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
			pop dx;
			pop cx;
		}
	#endif

	myImpl=Kernel::globalSysData->id;
}

Event::~Event(){
	SystemData data;
		data.name=data.EVE_DESC;
		data.id=myImpl;
		#ifndef BCC_BLOCK_IGNORE
			unsigned tcx = FP_SEG(&data);
			unsigned tdx = FP_OFF(&data);
			asm{
				push cx;
				push dx;
				mov cx, tcx;
				mov dx, tdx;
			}

			asm int 61h;   //pozivamo prekidnu ruitnu

			asm{
				pop dx;
				pop cx;
			}
		#endif
}

void Event::wait(){
	SystemData data;
		data.name=data.EVE_WAIT;
		data.id=myImpl;
		#ifndef BCC_BLOCK_IGNORE
			unsigned tcx = FP_SEG(&data);
			unsigned tdx = FP_OFF(&data);
			asm{
				push cx;
				push dx;
				mov cx, tcx;
				mov dx, tdx;
			}

			asm int 61h;   //pozivamo prekidnu ruitnu
			asm{
				pop dx;
				pop cx;
			}
		#endif
}

void Event::signal(){
	SystemData data;
	data.name=data.EVE_SIGNAL;
	data.id=myImpl;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
			push cx;
			push dx;
			mov cx, tcx;
			mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu
		asm{
			pop dx;
			pop cx;
		}
	#endif
}
