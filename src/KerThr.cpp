/*
 * KerThr.cpp
 *
 *  Created on: May 26, 2018
 *      Author: OS1
 */

#include "KerThr.h"
#include "Kernel.h"
#include "PCB.h"
#include "KerEven.h"
#include "KerSem.h"

KerThr::KerThr():Thread(-1) {}

KerThr::~KerThr() {}

void KerThr::run(){
  while(1){
	Kernel::softLock=1;
	#ifndef BCC_BLOCK_IGNORE
		asm sti;
	#endif
	switch(Kernel::globalSysData->name){
	case SystemData::THREAD_START:
		Kernel::listOfPCB->getByID(Kernel::globalSysData->id)->start();
		break;
	case SystemData::WAIT_TO_COMPLETE:
		Kernel::listOfPCB->getByID(Kernel::globalSysData->id)->waitToComplete();
		break;
	case SystemData::SLEEP:
		PCB::sleep(Kernel::globalSysData->timeToSleep);
		break;
	case SystemData::THREAD_DESC:
		Kernel::listOfPCB->getByID(Kernel::globalSysData->id)->waitToComplete();
		delete Kernel::listOfPCB->deleteByID(Kernel::globalSysData->id);
		break;
	case SystemData::THREAD_CONS:
		PCB* pcb= new PCB(Kernel::globalSysData->thread, Kernel::globalSysData->stackSize, Kernel::globalSysData->timeSlice);
		Kernel::globalSysData->id=pcb->getID();
		break;
	case SystemData::SEM_WAIT:
		Kernel::globalSysData->ret= Kernel::listOfKernelSem->getByID(Kernel::globalSysData->id)->wait(Kernel::globalSysData->toBlock);
		break;
	case SystemData::SEM_SIGNAL:
		Kernel::listOfKernelSem->getByID(Kernel::globalSysData->id)->signal();
		break;
	case SystemData::SEM_VAL:
		Kernel::globalSysData->semValue= Kernel::listOfKernelSem->getByID(Kernel::globalSysData->id)->val();
		break;
	case SystemData::SEM_DESC:
		delete Kernel::listOfKernelSem->deleteByID(Kernel::globalSysData->id);
		break;
	case SystemData::SEM_CONS:
		KernelSem *sem=new KernelSem(Kernel::globalSysData->semValue);
		Kernel::globalSysData->id=sem->getID();
		break;
	case SystemData::EVE_CONS:
		KernelEv* event=new KernelEv(Kernel::globalSysData->ivtNo);
		Kernel::globalSysData->id=event->getID();
		break;
	case SystemData::EVE_DESC:
		delete Kernel::listOfKernelEve->deleteByID(Kernel::globalSysData->id);
		break;
	case SystemData::EVE_WAIT:
		Kernel::listOfKernelEve->getByID(Kernel::globalSysData->id)->wait();
		break;
	case SystemData::EVE_SIGNAL:
		Kernel::listOfKernelEve->getByID(Kernel::globalSysData->id)->signal();
		break;
	case SystemData::DISPATCH:
		Kernel::dispatched=1;
		break;
	}
	#ifndef BCC_BLOCK_IGNORE
		asm int 63h;
	#endif
  }
}



