/*
 * IVTEn.cpp
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#include "IVTEn.h"
#include "KerEven.h"
#include <dos.h>
#include "STDLIB.H"
#include "Kernel.h"

IVTEntry* IVTEntry::IVTable[256]= { NULL };

IVTEntry::IVTEntry(unsigned char no ,InterruptFunc newIR){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	oldIR=NULL;
#ifndef BCC_BLOCK_IGNORE
	oldIR = getvect(no);
	setvect(no, newIR);
#endif
	ivtNo=no;
	IVTable[ivtNo]=this;
	event=NULL;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
}

IVTEntry::~IVTEntry(){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	IVTable[ivtNo]=NULL;
	oldIR();
#ifndef BCC_BLOCK_IGNORE
	setvect(ivtNo, oldIR);
	unlock();
#endif
}

void IVTEntry::signal(){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	if(IVTable[ivtNo]!=NULL)
		event->signal();
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
}

void IVTEntry::callOldInterrupt(){
	oldIR();
}

void IVTEntry::setEvent(KernelEv* e, unsigned char num){
	IVTEntry::IVTable[num]->event=e;
}

