/*
 * List.cpp
 *
 *  Created on: May 18, 2018
 *      Author: OS1
 */

#include "List.h"
#include "Kernel.h"
#include "PCB.h"
#include "SCHEDULE.H"

List::List() {
	first = NULL;
	last = NULL;
	len = 0;
}

List::~List() {
	while (first != NULL) {
		Elem* old = first;
		first = first->next;
		if(old) delete old;
	}
	first = NULL;
	last = NULL;
}

int List::getLen() {
	return len;
}

void List::decAll(){
	Elem *tek = first;
	PCB *pcb;
	while (tek!=NULL) {
		tek->info->decTimeToSleep();
		tek = tek->next;
	}

	deleteAllWith0();
}

void List::deleteAllWith0(){
	Elem *temp=first, *prev=NULL;
	PCB* pcb=NULL;
	while(temp!=NULL){
		if(temp->info->TimeToSleep()==0){
			if(temp==first){
				first = first->next;
				if(first==NULL) last=NULL;
				pcb=temp->info;
				#ifndef BCC_BLOCK_IGNORE
					lock();
				#endif
				delete temp;
				#ifndef BCC_BLOCK_IGNORE
					unlock();
				#endif
				temp=first;
			}//kraj uslova ako je prvi elem
			else{
				prev->next=temp->next;
				pcb=temp->info;
				temp->next=NULL;
				if(prev->next==NULL) last=prev;
				#ifndef BCC_BLOCK_IGNORE
					lock();
				#endif
					delete temp;
				#ifndef BCC_BLOCK_IGNORE
					unlock();
				#endif
				temp=prev->next;
			}
		}//kraj uslova da li je 0
		if(pcb!=NULL){
			pcb->setState(PCB::READY);
			Scheduler::put(pcb);
		}
		else{
			prev=temp;
			temp=temp->next;
		}
		pcb=NULL;
	}
}

void List::insertBack(PCB *pcb){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	Elem * newElem = new Elem(pcb);
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if (first == NULL)
		first = newElem;
	else
		last->next = newElem;
	last = newElem;
	len++;
}

void List::insertFront(PCB* pcb){
	Elem * newElem = new Elem(pcb);
	if (first == NULL){
			first = newElem;
			last=newElem;
		}
		else{
		newElem->next=first;
		first=newElem;
	}
	len++;
}

void List::insertSort(PCB* pcb){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	Elem * newElem = new Elem(pcb), *temp=first;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if(first==NULL){	//ako je ista prazna
		first = newElem;
		last=newElem;
	}
	else if(pcb->TimeToSleep() <= first->info->TimeToSleep()) { //ako je najmanji time ubaci na pocetak liste
		newElem->next=first;
		first=newElem;
	}
	else	//inace ako nije nista od toga
	{
		while((temp->next!=NULL) && (newElem->info->TimeToSleep()>temp->next->info->TimeToSleep()))
			temp=temp->next;
		newElem->next=temp->next;
		temp->next=newElem;
		if(newElem->next==NULL) last=newElem;
	}
	len++;
}

PCB* List::deleteFront(){
	PCB *pcb=NULL;
	if (first == NULL) {
		return NULL;
	} else {
		Elem* newElem = first;
		first = first->next;
		pcb=newElem->info;
		newElem->next = NULL;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
		delete newElem;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
		if (first == NULL)
			last = NULL;
		len--;
		return pcb;
}
}

PCB* List::deleteByID(int i){
	Elem *temp=first, *prev=NULL;
	PCB* pcb=NULL;
	for(; temp!=NULL; temp=temp->next){
		if(i==temp->info->getID())
			break;
		prev=temp;
	}
	if(temp==NULL){
		return pcb;
}
	len--;
	if(prev==NULL){
		first=first->next;
		if(first==NULL) last=NULL;
		pcb=temp->info;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
		delete temp;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
		if(len==0)first=NULL;
		return pcb;
	}
	prev->next=temp->next;
	temp->next=NULL;
	pcb=temp->info;
	if(prev->next==NULL) last=prev;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete temp;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if(len==0)first=NULL;
	return pcb;
}

PCB* List::getByID(int i){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	Elem *temp=first;
	for(;temp!=NULL;temp=temp->next){
		if(temp->info->getID()==i)
			break;
	}
	if(temp==NULL) {
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
		return NULL;
	}
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	return temp->info;
}
