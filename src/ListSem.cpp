/*
 * ListSemSem.cpp
 *
 *  Created on: May 26, 2018
 *      Author: OS1
 */

#include "ListSem.h"
#include "SCHEDULE.H"
#include "KerSem.h"
#include "Kernel.h"

ListSem::ListSem() {
	first = NULL;
	last = NULL;
	len = 0;
}

ListSem::~ListSem() {
	while (first != NULL) {
		Elem* old = first;
		first = first->next;
		delete old;
	}
	first = NULL;
	last = NULL;
}

int ListSem::getLen() {
	return len;
}


void ListSem::insertBack(KernelSem *pcb){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	Elem * newElem = new Elem(pcb);
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if (first == NULL)
		first = newElem;
	else
		last->next = newElem;
	last = newElem;
	len++;
}

void ListSem::insertFront(KernelSem* pcb){
	Elem *newElem = new Elem(pcb);
	if (first == NULL){
			first = newElem;
			last=newElem;
		}
		else{
		newElem->next=first;
		first=newElem;
	}
	len++;
}


KernelSem* ListSem::deleteFront(){
	KernelSem *pcb=NULL;
	if (first == NULL) {
		return NULL;
	} else {
		Elem* newElem = first;
		first = first->next;
		pcb=newElem->info;
		newElem->next = NULL;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
		delete newElem;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
		if (first == NULL)
			last = NULL;
		len--;
		return pcb;
}
}

KernelSem* ListSem::deleteByID(int i){
	Elem *temp=first, *prev=NULL;
	KernelSem* pcb=NULL;
	for(; temp!=NULL; temp=temp->next){
		if(i==temp->info->getID())
			break;
		prev=temp;
	}
	if(temp==NULL){
		return pcb;
}
	len--;
	if(prev==NULL){
		first=first->next;
		if(first==NULL) last=NULL;
		pcb=temp->info;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
		delete temp;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if(len==0)first=NULL;
		return pcb;
	}
	prev->next=temp->next;
	temp->next=NULL;
	pcb=temp->info;
	if(prev->next==NULL) last=prev;
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	delete temp;
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	if(len==0)first=NULL;
	return pcb;
}

KernelSem* ListSem::getByID(int i){
	Elem *temp=first;
	for(;temp!=NULL;temp=temp->next){
		if(temp->info->getID()==i)
			break;
	}
	if(temp==NULL) {
		return NULL;
	}
	return temp->info;
}
