/*
 * KerSem.cpp
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#include "KerSem.h"
#include "PCB.h"
#include "SCHEDULE.H"
#include "Kernel.h"
#include "List.h"

int KernelSem::ID=0;

KernelSem::KernelSem(int init){
	id=++ID;
	value=init;
	queue=new Queue();
	Kernel::listOfKernelSem->insertBack(this);
}

KernelSem::~KernelSem(){
	delete queue;
}

int KernelSem::wait(int toBlock){
	int ret=0;
	if(!toBlock && (value<=0))
		ret=-1;
	else
		if(--value<0){
			ret=1;
			Kernel::running->state=PCB::BLOCKED;
			queue->put((PCB*)Kernel::running);
			Kernel::dispatched=1;
		}
return ret;
}

void KernelSem::signal(){
	PCB* pcb;
	if(value++<0){
		pcb=queue->get();
		pcb->setState(PCB::READY);
		Scheduler::put(pcb);
	}

}

int KernelSem::val() const{
	return value;
}
