/*
 * Thread.cpp
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */

#include "Thread.h"
#include "PCB.h"
#include "SCHEDULE.H"
#include "Kernel.h"


Thread::Thread(int x){   //konstruktor za nit main tj startingThread
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	PCB* pcb= new PCB(this, 4096, 2);
	myPCB= pcb->getID();
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
}

Thread::Thread(StackSize stackSize, Time timeSlice){
	SystemData data;
	data.name=data.THREAD_CONS;
	data.thread=this;
	data.stackSize=stackSize;
	data.timeSlice=timeSlice;
#ifndef BCC_BLOCK_IGNORE
	unsigned tcx = FP_SEG(&data);
	unsigned tdx = FP_OFF(&data);
	asm{
	push cx;
	push dx;
	mov cx, tcx;
	mov dx, tdx;
	}

	asm int 61h;   //pozivamo prekidnu ruitnu

	asm{
		pop dx;
		pop cx;
	}
#endif
	myPCB=Kernel::globalSysData->id;
}

Thread::~Thread(){
#ifndef BCC_BLOCK_IGNORE
	lock();
#endif
	if((myPCB==1) || (myPCB==2)){
		if(myPCB==1) waitToComplete();
		delete Kernel::listOfPCB->deleteByID(myPCB);
		return;
	}
#ifndef BCC_BLOCK_IGNORE
	unlock();
#endif
	SystemData data;
	data.name=data.THREAD_DESC;
	data.id=myPCB;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
				pop dx;
				pop cx;
			}
	#endif
}

void Thread::start(){
	SystemData data;
	data.name=data.THREAD_START;
	data.id=myPCB;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
			pop dx;
			pop cx;
		}
	#endif
}

void Thread::waitToComplete(){
	SystemData data;
	data.name=data.WAIT_TO_COMPLETE;
	data.id=myPCB;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu
		asm{
			pop dx;
			pop cx;
		}
	#endif
}

void Thread::sleep(Time timeToSleep){
	SystemData data;
	data.name=data.SLEEP;
	data.timeToSleep=timeToSleep;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
			pop dx;
			pop cx;
		}
	#endif
}

void dispatch(){
	SystemData data;
	data.name=data.DISPATCH;
#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
			pop dx;
			pop cx;
		}
	#endif
}

void Thread::wrapper(Thread* running){
	running->run();

	Kernel::listOfPCB->getByID(running->myPCB)->state=PCB::OVER; //zavrsen run

	while(!Kernel::listOfPCB->getByID(running->myPCB)->waitingThread->empty()){
	Kernel::listOfPCB->getByID(running->myPCB)->waitingThread->getFirst()->state=PCB::READY;
	Scheduler::put((Kernel::listOfPCB->getByID(running->myPCB)->waitingThread->get()));
	}

	dispatch(); //promjena konteksta, nikad se ne izlazi iz wrappera
}
