/*
 * Semaphor.cpp
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#include "Semaphor.h"
#include "KerSem.h"
#include "Kernel.h"

Semaphore::Semaphore(int init){
	SystemData data;
	data.name=data.SEM_CONS;
	data.semValue=init;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
			pop dx;
			pop cx;
		}
	#endif
	myImpl=Kernel::globalSysData->id;
}

Semaphore::~Semaphore(){
	SystemData data;
	data.name=data.SEM_DESC;
	data.id=myImpl;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu
		asm{
			pop dx;
			pop cx;
		}
	#endif

}

int Semaphore::wait(int toBlock){
	SystemData data;
	data.name=data.SEM_WAIT;
	data.id=myImpl;
	data.toBlock=toBlock;
	#ifndef BCC_BLOCK_IGNORE
		unsigned tcx = FP_SEG(&data);
		unsigned tdx = FP_OFF(&data);
		asm{
		push cx;
		push dx;
		mov cx, tcx;
		mov dx, tdx;
		}

		asm int 61h;   //pozivamo prekidnu ruitnu

		asm{
			pop dx;
			pop cx;
		}
	#endif

	return Kernel::globalSysData->ret;
}

void Semaphore::signal(){
	SystemData data;
		data.name=data.SEM_SIGNAL;
		data.id=myImpl;
		#ifndef BCC_BLOCK_IGNORE
			unsigned tcx = FP_SEG(&data);
			unsigned tdx = FP_OFF(&data);
			asm{
			push cx;
			push dx;
			mov cx, tcx;
			mov dx, tdx;
			}

			asm int 61h;   //pozivamo prekidnu ruitnu

			asm{
				pop dx;
				pop cx;
			}
		#endif
}

int Semaphore::val() const{
	SystemData data;
		data.name=data.SEM_VAL;
		data.id=myImpl;
		#ifndef BCC_BLOCK_IGNORE
			unsigned tcx = FP_SEG(&data);
			unsigned tdx = FP_OFF(&data);
			asm{
			push cx;
			push dx;
			mov cx, tcx;
			mov dx, tdx;
			}

			asm int 61h;   //pozivamo prekidnu ruitnu

			asm{
				pop dx;
				pop cx;
			}
		#endif
	return Kernel::globalSysData->semValue;
}
