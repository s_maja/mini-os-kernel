/*
 * IVTEn.h
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#ifndef IVTEN_H_
#define IVTEN_H_

typedef void interrupt (*InterruptFunc)(...);  //imam isto u krenel pa vidi smije li

class KernelEv;
class Event;

class IVTEntry{
	friend class KernelEv;
public:
	IVTEntry(unsigned char ,InterruptFunc);
   ~IVTEntry();
	void signal();
	void callOldInterrupt();
	static void setEvent(KernelEv*, unsigned char);

private:
	static IVTEntry* IVTable[];
	unsigned char  ivtNo;
	KernelEv* event;
	InterruptFunc oldIR;
};

#endif /* IVTEN_H_ */
