/*
 * KerThr.h
 *
 *  Created on: May 26, 2018
 *      Author: OS1
 */

#ifndef KERTHR_H_
#define KERTHR_H_

#include "Thread.h"

class KerThr: public Thread {
public:
	KerThr();
	virtual ~KerThr();

	void run();
	//void start(); ms da ne treba jer se isto startuje kao obicne
};

#endif /* KERTHR_H_ */
