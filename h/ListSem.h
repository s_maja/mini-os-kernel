/*
 * ListSem.h
 *
 *  Created on: May 26, 2018
 *      Author: OS1
 */

#ifndef LISTSEM_H_
#define LISTSEM_H_

#include "STDLIB.H"

class KernelSem;

class ListSem {
private:
	struct Elem {
		KernelSem* info;
		Elem *next;
		Elem(KernelSem *sem) {
			info = sem;
			next = NULL;
		}
	};

	Elem *first, *last;
	int len;
public:
	ListSem();
	~ListSem();

	int getLen();

	void insertBack(KernelSem*);
	void insertFront(KernelSem*);

	KernelSem* deleteFront();
	KernelSem* deleteByID(int);

	KernelSem* getByID(int);
};
#endif /* LISTSEM_H_ */
