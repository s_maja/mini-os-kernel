/*
 * PCB.h
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */

#ifndef PCB_H_
#define PCB_H_

#include <iostream.h>
#include "Thread.h"
#include "Queue.h"

class Queue;

class PCB {
public:
	enum State {
		NEW, READY, BLOCKED, OVER
	};

	PCB(Thread*, StackSize, Time);
    ~PCB();
    void start();
    void waitToComplete();
    static void sleep(unsigned int);

    ID getID() { return id; }
    long TimeToSleep() {return timeToSleep;}
    void decTimeToSleep() { timeToSleep--;}
    void setState(State s) { state=s;}
    volatile enum State state;
	ID id;
private:
    void initStack();
    friend class Thread;
    friend class Kernel;
    friend class Idle;

    static ID PID;

	unsigned int ss, sp;

	StackSize stackSize;
	long timeSlice;
	volatile long timePassed;
	volatile long timeToSleep;
	Thread *myThread;
	unsigned unsigned int *stack;
	Queue *waitingThread;  //red niti koje cekaju da se zavrsi ova nit
};

#endif /* PCB_H_ */
