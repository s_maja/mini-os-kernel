/*
 * KerEven.h
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#ifndef KEREVEN_H_
#define KEREVEN_H_

#include "Event.h"

class PCB;

class KernelEv {
public:
	KernelEv(IVTNo ivtNo);
	~KernelEv();
	void wait();
	void signal();

	int getID(){ return id;}
private:
	static int ID;
	int id;
	PCB* owner;
	IVTNo ivtNum;
	int val;
};

#endif /* KEREVEN_H_ */
