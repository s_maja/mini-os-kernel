/*
 * Event.h
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#ifndef EVENT_H_
#define EVENT_H_

#include "IVTEn.h"
#include "Kernel.h"

typedef unsigned char IVTNo;
class KernelEv;
int syncPrintf(const char *format, ...);

#define PREPAREENTRY(ivtno,old) \
void interrupt routine##ivtno(...); \
IVTEntry ent##ivtno(ivtno,routine##ivtno); \
void interrupt routine##ivtno(...) { \
	ent##ivtno.signal(); \
	if (old) ent##ivtno.callOldInterrupt(); \
}

class Event {
public:
	Event(IVTNo ivtNo);
	~Event();
	void wait();

protected:
	friend class KernelEv;
	void signal(); // can call KernelEv

private:
	int myImpl;
};

#endif /* EVENT_H_ */
