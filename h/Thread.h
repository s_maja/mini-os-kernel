/*
 * Thread.h
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <iostream.h>

typedef unsigned long StackSize;
const StackSize defaultStackSize = 4096;

typedef unsigned int Time; // time, x 55ms
const Time defaultTimeSlice = 2; // default = 2*55ms

typedef int ID;

class PCB;

class Thread {
public:
	void start();
	void waitToComplete();
	virtual ~Thread();
	static void sleep(Time timeToSleep);

	int getId(){ return myPCB;}

protected:
	Thread(StackSize stackSize = defaultStackSize, Time timeSlice = defaultTimeSlice);
	virtual void run() {}

private:
	ID myPCB;
	static void wrapper(Thread* running);
	Thread(int);

	friend class PCB;
	friend class Kernel;
	friend class Idle;
	friend class KerThr;
};

void dispatch();

#endif /* THREAD_H_ */
