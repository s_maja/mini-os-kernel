/*
 * Kernel.h
 *
 *  Created on: May 14, 2018
 *      Author: OS1
 */

#ifndef KERNEL_H_
#define KERNEL_H_

#include <stdlib.h>
#include "PCB.h"
#include "Thread.h"
#include "Idle.h"
#include "KerThr.h"
#include "List.h"
#include "ListSem.h"
#include "ListEve.h"
#include "Semaphor.h"
#ifndef BCC_BLOCK_IGNORE
#include <dos.h>
#endif


typedef void interrupt(*InterruptFunc)(...);
#define lock() {asm{pushf; cli;}}
#define unlock() {asm{popf}}


struct SystemData{
	enum Functions{
		THREAD_START, WAIT_TO_COMPLETE, SLEEP, THREAD_DESC, THREAD_CONS,
		SEM_WAIT, SEM_SIGNAL ,SEM_VAL,SEM_DESC,SEM_CONS,
		EVE_CONS, EVE_DESC, EVE_WAIT, EVE_SIGNAL, DISPATCH
	};

	Functions name;
	int id;
	Thread *thread;
	unsigned long stackSize;
	unsigned int timeSlice;
	unsigned int timeToSleep;
	int semValue;
	int toBlock;
	unsigned char ivtNo;
	int ret;
};

class Kernel {
public:
	static volatile PCB* running;
	static Idle* idleThread;
	static Thread* startingThread;
	static KerThr* runningKernelThread;

	static volatile int dispatched;
	static volatile int softLock;
	static InterruptFunc oldTimer;
	static SystemData* globalSysData;

	static List* listOfPCB;
	static ListSem* listOfKernelSem;
	static ListEve* listOfKernelEve;
	static List* sleepingThreads;

	friend class PCB;
public:
	Kernel(){}
	~Kernel();

	static void inic();
	static void restore();
	static void interrupt timerInterrupt(...);
	static void interrupt SysCallRout(...);
	static void interrupt BackToUserRun(...);

};

#endif /* KERNEL_H_ */
