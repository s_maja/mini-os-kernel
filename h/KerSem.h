/*
 * KerSem.h
 *
 *  Created on: May 19, 2018
 *      Author: OS1
 */

#ifndef KERSEM_H_
#define KERSEM_H_

#include "Queue.h"

class KernelSem {
public:
	KernelSem(int init);
	virtual ~KernelSem();
	virtual int wait(int toBlock);
	virtual void signal();
	int val() const;
	int getID(){ return id;}
private:
	static int ID;
	int id;
	int value;
	Queue* queue;
};

#endif /* KERSEM_H_ */
