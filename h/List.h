/*
 * List.h
 *
 *  Created on: May 18, 2018
 *      Author: OS1
 */

#ifndef LIST_H_
#define LIST_H_

#include "PCB.h"

class List {
private:
	struct Elem {
		PCB* info;
		Elem * next;
		Elem(PCB *pcb) {
			info = pcb;
			next = NULL;
		}
	};

	Elem *first, *last;
	int len;
public:
	List();
	~List();

	int getLen();

	void insertBack(PCB*);
	void insertFront(PCB*);
	void insertSort(PCB*);

	PCB* deleteFront();
	PCB* deleteByID(int);
	void deleteAllWith0();

	void decAll();
	PCB* getByID(int);
};

#endif /* LIST_H_ */
