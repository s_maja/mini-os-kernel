/*
 * Queue.h
 *
 *  Created on: May 9, 2018
 *      Author: OS1
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include "PCB.h"

class Queue {
private:
	struct Elem {
		PCB* info;
		Elem * next;
		Elem(PCB *pcb) {
			info = pcb;
			next = NULL;
		}
	};

	Elem *first, *last;
	int len;
public:
	Queue();
	~Queue();

	PCB* getFirst();
	PCB* get();
	void put(PCB*);

	int empty();
	int getLen();
};

#endif /* QUEUE_H_ */
