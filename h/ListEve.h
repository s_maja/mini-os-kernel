/*
 * ListEve.h
 *
 *  Created on: May 26, 2018
 *      Author: OS1
 */

#ifndef LISTEVE_H_
#define LISTEVE_H_

#include "STDLIB.H"

class KernelEv;

class ListEve {
private:
	struct Elem {
		KernelEv* info;
		Elem *next;
		Elem(KernelEv *sem) {
			info = sem;
			next = NULL;
		}
	};

	Elem *first, *last;
	int len;
public:
	ListEve();
	~ListEve();

	int getLen();

	void insertBack(KernelEv*);
	void insertFront(KernelEv*);

	KernelEv* deleteFront();
	KernelEv* deleteByID(int);

	KernelEv* getByID(int);
};

#endif /* LISTEVE_H_ */
